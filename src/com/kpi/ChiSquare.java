package com.kpi;

import java.util.*;

/**
 * Class of ChiSquare, where we take an value of Chi from two samples
 * @author Yuriy
 * @version 1.01
 */
public class ChiSquare {
    private double[] sample1;
    private double[] sample2;
    private ArrayList<Integer> sampleSplits1;
    private ArrayList<Integer> sampleSplits2;

    /**
     * Getter of sample 1 array
     * @return
     * Value of field of sample1
     */
    public double[] getSample1() {
        return sample1;
    }

    /**
     * Setter of value of field Sample1
     * @param sample1
     * A value, that we are going to set in this field
     */
    public void setSample1(double[] sample1) {
        this.sample1 = sample1;
    }

    /**
     * Getter of sample 2 array
     * @return
     * Value of field of sample2
     */
    public double[] getSample2() {
        return sample2;
    }

    /**
     * Setter of value of field Sample2
     * @param sample2
     * A value, that we are going to set in this field
     */
    public void setSample2(double[] sample2) {
        this.sample2 = sample2;
    }

    /**
     * Setter of ArrayList of splits, that makes a number of values that is in intervals
     * @param sampleSplits1
     * A number of values of Sample1, that is in intervals
     */
    public void setSampleSplits1(ArrayList<Integer> sampleSplits1) {
        this.sampleSplits1 = sampleSplits1;
    }

    /**
     * Getter of ArrayList of splits, that we take from field of this class
     * @return
     * A value of all numbers, that is in intervals of Sample1
     */
    public ArrayList<Integer> getSampleSplits1() {
        return sampleSplits1;
    }

    /**
     * Setter of ArrayList of splits, that makes a number of values that is in intervals
     * @param sampleSplits2
     * A number of values of Sample2, that is in intervals
     */
    public void setSampleSplits2(ArrayList<Integer> sampleSplits2){
        this.sampleSplits2 = sampleSplits2;
    }

    /**
     * Getter of ArrayList of splits, that we take from field of this class
     * @return
     * A value of all numbers, that is in intervals of Sample1
     */
    public ArrayList<Integer> getSampleSplits2() {
        return sampleSplits2;
    }

    /**
     * Method, that sorts arrays, what we enter to this class
     * @param array
     * Array, that will be sorted using Arrays.sort QuickSort method
     * @return
     * A Sorted array, using this method
     */
    public double[] sortArray(double[] array){
        Arrays.sort(array);
        return array;
    }

    /**
     * Constructor of class ChiSquare, which saves a values we entered
     * @param sam1
     * A value of sample1 array
     * @param sam2
     * A value of sample2 array
     */
    public ChiSquare(double[] sam1, double[] sam2){
     setSample1(sortArray(sam1));
     setSample2(sortArray(sam2));
    }

    /**
     * Method, which sums all elements in this ArrayList
     * @param arr
     * ArrayList, whose values is needed to sum
     * @return
     * A sum of all elements in this array
     */
    public int sumArray(ArrayList<Integer> arr){
        int sum = 0;
        for (int i = 0; i < arr.size(); i++) {
            sum += arr.get(i);
        }
        return sum;
    }

    /**
     * A method, that makes splits for Sample1 array. It uses randomly generated numbers
     * @param n
     * A number of intervals, that we need to make a splits
     */
    public void setNewSampleSplits1(int n){
        ArrayList<Integer> buff = new ArrayList<>();
        int buffer = 0;
        for (int i = 0; i < n; i++){
            if(i == 0){
                buff.add(boundOfIntervals(getSample1().length));
            }
            else if(i == n - 1){
                buff.add(getSample1().length - sumArray(buff));
            }
            else{
                buff.add(boundOfIntervals(getSample1().length - buffer));
            }
            buffer += buff.get(i);
        }
        setSampleSplits1(buff);
    }

    /**
     * A method, that generates a randomly value for bounds of intervals of all splits
     * @param l
     * A number-bound for generator, that need to make a random number
     * @return
     */
    public int boundOfIntervals (int l){
        return (int)((Math.random()*(l - 2))+1);
    }

    /**
     * Method, where we make a buffer array for intermediate numbers of splits
     * @param array
     * Array, that we take to make an intermediate numbers
     * @return
     * An array of ready intermediate numbers
     */
    public double[] bufferedArray (double[] array){
        double[] result = new double[getSampleSplits1().size() - 1];
        int buffer = 0;
        for(int i = 0; i < result.length; i++){
            if (i == 0) {
                result[i] = (array[getSampleSplits1().get(i) - 1] + array[getSampleSplits1().get(i)]) / 2;
            }
            else {
                result[i] = (array[getSampleSplits1().get(i) + buffer - 1] + array[getSampleSplits1().get(i) + buffer]) / 2;
            }
            buffer += getSampleSplits1().get(i);
        }
        return result;
    }

    /**
     * A method, that makes splits for Sample2 array. It uses value from first Sample1 array
     * @param k
     * A number of intervals, that we need to make a splits
     */
    public void setNewSampleSplits2(int k){
        setNewSampleSplits1(k);
        double[] buffArr = getSample1();
        double[] buffArr2 = getSample2();
        ArrayList<Integer> buffList = new ArrayList<>();
        double[] buffer = bufferedArray(buffArr);
        int buffer2 = 0;
        for (int i = 0; i < buffer.length; i++) {
            for (int j = 0; j < buffArr2.length; j++) {
                if (buffer[i] < buffArr2[j]) {
                    if (i == 0) {
                        buffList.add(j);
                    } else {
                        buffList.add(j - buffer2);
                    }
                    break;
                }
                if (buffer[i] > buffArr2[buffArr2.length - 1])
                    throw new UnsupportedOperationException("Second array can't be used for this calculation!");
                }
            buffer2 += buffList.get(i);
        }
        if(buffList.size() == buffer.length){
            buffList.add(buffArr2.length - sumArray(buffList));
        }
        setSampleSplits2(buffList);
    }

    /**
     * Method of ChiSquare, where we take a value of ChiSquare, to understand the probability of this samples
     * @param k
     * A number of splits to intervals, that we make for Samples
     * @return
     * A value of Chi, to take it form probability part
     */
    public double chiExp(int k){
        setNewSampleSplits2(k);
        double sum = 0;
        double[] arr1 = getSample1();
        double[] arr2 = getSample2();
        ArrayList<Integer> split1 = getSampleSplits1();
        ArrayList<Integer> split2 = getSampleSplits2();
        for (int i = 0; i < getSampleSplits1().size(); i++){
            sum += Math.pow((double)split1.get(i)/arr1.length - (double)split2.get(i)/arr2.length ,  2) / (split1.get(i) + split2.get(i));
        }
        sum = sum * arr1.length * arr2.length;
        return sum;
    }
}