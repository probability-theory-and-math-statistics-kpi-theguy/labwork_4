package com.kpi;

import java.util.*;
import java.io.*;
import static java.lang.System.*;

/**
 * Main class of the checking by method of Chi Square
 * @author Yuriy
 * @version 1.01
 */
public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scan = new Scanner(in);
        out.println("We have chi-squared test for two samples. Enter the length of it:");
        int n = scan.nextInt();
        out.println("How many intervals, do you like to split in the samples? We use only 2, 3 or 4.");
        int m = scan.nextInt();
        out.println("Would you like to enter it's from yourself or not?:");
        out.println("1. - you enter the numbers for samples;");
        out.println("2. - samples will be generated automatically.");
        int ch = scan.nextInt();
        double[] sample1 = new double[n];
        double[] sample2 = new double[n];
        if(ch == 1){
            out.println("Enter the value of first sample:");
            for(int i = 0; i < n; i++){
                out.print(i+". ");
                sample1[i] = scan.nextDouble();
                out.println();
            }
            out.println("Enter the value of second sample:");
            for(int i = 0; i < n; i++){
                out.print(i+". ");
                sample2[i] = scan.nextDouble();
                out.println();
            }
        }
        else{
            out.println("We have randomly created samples.");
            for (int i = 0; i < n; i++){
                sample1[i] = (int)(((Math.random()*2)-1)*500);
            }
            for (int i = 0; i < n; i++){
                sample2[i] = (int)(((Math.random()*2)-1)*500);
            }
        }
        out.println("Our samples with size of "+n+" is saved in file: С://generator3.txt");
        File fil = new File("A://generator3.txt");
        try {
            if(!fil.exists()){
                fil.createNewFile();
            }
            BufferedWriter buff = new BufferedWriter(new FileWriter(fil));
            for(int i = 0; i < sample1.length; i++){
                buff.write(sample1[i] + " ");
            }
            buff.write("         2: ");
            for(int i = 0; i < sample2.length; i++){
                buff.write(sample2[i] + " ");
            }
            buff.flush();
            buff.close();
        }catch(IOException e){
            err.println(e.getMessage());
        }
        out.println("So, we have a value of chi square here:");
        ChiSquare chi = new ChiSquare(sample1, sample2);
        double c = chi.chiExp(m);
        out.println("Chi = "+c);
        Probability prob = new Probability(c, m);
        out.println("Probability, that these two samples is in one distribution: "+prob.chooseProbability());
    }
}
