package com.kpi;

/**
 * Class of probability, where we take a probability to make from samples an distribution
 * @author Yuriy
 * @version 1.01
 */
public class Probability {
    private double Chi;
    private double[][] probMatrix = {{0, 0.001, 0.004, 0.016, 0.064, 0.158, 0.455, 1.074, 1.642, 2.71, 3.84}, {0.02, 0.04, 0.103, 0.211, 0.446, 0.713, 1.386, 2.41, 3.22, 4.6, 5.99}, {0.115, 0.185, 0.352, 0.584, 1.005, 1.424, 2.37, 3.66, 4.63, 6.25, 7.82}};
    private double[] probArray = {0.01, 0.05, 0.1, 0.2, 0.3, 0.5, 0.7, 0.8, 0.9, 0.95};
    private int L;

    /**
     * Getter of value of ChiSquare, that is saved in field of this class
     * @return
     * A value of ChiSquare from this class
     */
    public double getChi() {
        return Chi;
    }

    /**
     * Setter of value of ChiSquare, that we needs to save in this class
     * @param chi
     * A value of this Chi
     */
    public void setChi(double chi) {
        this.Chi = chi;
    }

    /**
     * Getter of ProbabilityMatrix, a constant value taken from table
     * @return
     * A value of this field
     */
    public double[][] getProbMatrix() {
        return probMatrix;
    }

    /**
     * Getter of ProbabilityArray, a constant value taken from table
     * @return
     * A value of this field
     */
    public double[] getProbArray() {
        return probArray;
    }

    /**
     * Getter of L, a number of intervals, which we made in ChiSquare class
     * @return
     * A value from this field
     */
    public int getL() {
        return L;
    }

    /**
     * Setter of L, a number of intervals, that we made in ChiSquare class
     * @param l
     */
    public void setL(int l) {
        this.L = l;
    }

    /**
     * Probability constructor whic sets a values for fields of this class
     * @param c
     * A value of ChiSquare to save in field
     * @param l
     * A value of l-intervals to save in field
     */
    public Probability(double c, int l){
        setChi(c);
        setL(l);
    }

    /**
     * A method to choose a probability for this type of ChiSquare value
     * @return
     * A value of probability of this samples
     */
    public double chooseProbability(){
        double c = getChi();
        int l = getL() - 2;
        double result = 0;
        int number = 0;
        for (int i = 0; i < getProbMatrix()[l].length; i++){
            if(Math.abs(getProbMatrix()[l][i] - c) < Math.abs(getProbMatrix()[l][number] - c)){
                number = i;
            }
            else if(Math.abs(getProbMatrix()[l][i] - c) > Math.abs(getProbMatrix()[l][number] - c)){
                result = getProbArray()[i - 2];
                break;
            }
        }
        return 1 - result;
    }

}